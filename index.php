
<?php

error_reporting( 0 );

require_once __DIR__ . '/vendor/autoload.php';

session_start();
define('APPLICATION_NAME', 'Google Calendar API PHP Quickstart');
define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret.json');
// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/calendar-php-quickstart.json
define('SCOPES', implode(' ', array(
  Google_Service_Calendar::CALENDAR_READONLY)
));

  $client = new Google_Client();
  $client->setApplicationName(APPLICATION_NAME);
  $client->setScopes(SCOPES);
  $client->setAuthConfig(CLIENT_SECRET_PATH);
  $client->setAccessType('offline');
  $client->setRedirectUri('http://localhost/tmp/php-google-calendar/index.php');

  // Load previously authorized credentials from a file.
  if(isset($_GET['code'])) {
      // Exchange authorization code for an access token.
      $accessToken = $client->fetchAccessTokenWithAuthCode($_GET['code']);

      $f = fopen("token.inc", "x+");
      fwrite($f, $accessToken['access_token']);
      fclose($f);

      header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
  }


?>

<!DOCTYPE html>
<html>
<head>
  <title>
    Google Calendar Events
  </title>
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.14/angular.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
  <style type="text/css">
    body, h1, h2, h3, h4, h5, h6, p, a {
      font-family: 'Lato', sans-serif;
    }
  </style>
</head>
<body>
  <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Google Calendar Demo</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">View Calendar</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="container">

      <div class="starter-template">
        <h1>Events</h1>
        <div class="row" id="content" ng-app="myApp" ng-controller="myCtrl">
            <div ng-show="data" class="col-md-4" ng-repeat="d in data">
              <div class="panel panel-success">
                <div class="panel-heading">{{d.date}}</div>
                <div class="panel-body">
                  <div class="event" ng-repeat="e in d.events">
                    <h4>{{e.name}}</h4>
                    <p>{{e.startTime != '00:00' ? e.startTime : ''}} - {{e.endTime != '00:00' ? e.endTime : ''}}</p>
                    <hr>
                  </div>
                </div>
              </div>
          </div>
        </div>
        
        <?php
          if (file_exists("token.inc")) {
            $client->setAccessToken(file_get_contents("token.inc"));

            $service = new Google_Service_Calendar($client);

          // Print the next 10 events on the user's calendar.
            $calendarId = 'primary';
            $optParams = array(
              'maxResults' => 10,
              'orderBy' => 'startTime',
              'singleEvents' => TRUE,
              'timeMin' => date('c')
            );

            $results = $service->events->listEvents($calendarId, $optParams);

            if (count($results->getItems()) == 0) {
              echo "No upcoming events found.\n";
            } else {
              
              $array = array();

              foreach ($results->getItems() as $event) {
              
             
                $start = $event->start->dateTime;
                if (empty($start)) {
                  $start = $event->start->date;
                }

                $end = $event->end->dateTime;
                if (empty($end)) {
                  $end = $event->end->date;
                }

                array_push($array, array("name" => $event->getSummary(), "start" => $start, "end" => $end));
              }

            ?>
            <script type="text/javascript">
            <?php echo "var eventData = " . json_encode($array) . ";"; ?>
            console.log(eventData);
            /** Group dates */
            var dates = [], finalData = [];
            for (var i = 0; i < eventData.length; i++) {
              var tmp = moment(eventData[i].start).format("MMM Do");
              if(dates.indexOf(tmp) === -1) {
                dates.push(tmp);
                finalData.push({date: tmp, events: []});
              }
            }

            for (var i = 0; i < eventData.length; i++) {
              var tmp = moment(eventData[i].start).format("MMM Do");
              var idx = dates.indexOf(tmp);
              finalData[idx].events.push(eventData[i]);
            }
            console.log("FINL >> ", finalData);
            var app = angular.module('myApp', []);

            app.controller('myCtrl', ['$scope', function ($scope) {
              $scope.data = finalData;

              $scope.myInterval = 5000;
              $scope.noWrapSlides = false;
              $scope.active = 0;

              angular.forEach($scope.data, function (o, i) {
                angular.forEach(o.events, function (e, idx) {
                  e.startTime = moment(e.start).format("HH:mm");
                  e.endTime = moment(e.end).format("HH:mm");
                });
              });
            }]);
            </script>
            <?php }
          } else {
            // Request authorization from the user.
            $authUrl = $client->createAuthUrl();
            echo "<a class='btn btn-info' href='$authUrl'>Login</a>";
          }
        ?>

      </div>

    </div>
</body>
</html>
