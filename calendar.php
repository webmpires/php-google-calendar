<?php
	
	// echo "<pre>";
	// print_r($_GET);
	require_once __DIR__ . '/vendor/autoload.php';
	session_start();
	define('APPLICATION_NAME', 'Google Calendar API PHP Quickstart');
	define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret.json');
	// If modifying these scopes, delete your previously saved credentials
	// at ~/.credentials/calendar-php-quickstart.json
	define('SCOPES', implode(' ', array(
	  Google_Service_Calendar::CALENDAR_READONLY)
	));

	$client = new Google_Client();

	$client->setApplicationName(APPLICATION_NAME);
	$client->setScopes(SCOPES);
	$client->setAuthConfig(CLIENT_SECRET_PATH);
	$client->setAccessType('offline');
	$client->setRedirectUri('http://localhost/tmp/php-google-calendar/calendar.php');

	$client->setAccessToken($_SESSION["accessToken"]);

	// Refresh the token if it's expired.
	if ($client->isAccessTokenExpired()) {
		$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
	}

  	$service = new Google_Service_Calendar($client);

	// Print the next 10 events on the user's calendar.
	$calendarId = 'primary';
	$optParams = array(
	  'maxResults' => 10,
	  'orderBy' => 'startTime',
	  'singleEvents' => TRUE,
	  'timeMin' => date('c')
	);

	$results = $service->events->listEvents($calendarId, $optParams);

	if (count($results->getItems()) == 0) {
	  echo "No upcoming events found.\n";
	} else {
	  echo "Upcoming events:\n";
	  foreach ($results->getItems() as $event) {
		$start = $event->start->dateTime;
		if (empty($start)) {
		  $start = $event->start->date;
		}
		echo $event->getSummary() . " "  . $start . "\n";
	  }
	}

?>